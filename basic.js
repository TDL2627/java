// // basic varibles
 let x=9;
console.log(x);
// // let allows change in varibles
// // const refuses change
 const y = 7;
console.log(y);
console.log(x + 10);
console.log(y-1);
console.log(x+y);

// // if else
let a1=2;
let a2=4;
if(a1<a2){
    console.log("well done") }
else{
    console.log("done bad")
};
// // boolean
 if(a1==a2){
    console.log("nice")       
 }
 else{console.log("\t meh ")};
// // the code == checks value not data type

 let b1=2;
 let b2="2";
 if (b1==b2){
     console.log("facts \n")
 };
// // the if dont need else

 if (b1===b2){console.log("Absolutely True")}else{console.log("flipping false\n")};
// // === checks value and data type
// // != checks not equal (==)
 if(b1!=b2){console.log("not equal \n")} else{console.log("equal \n")};
// // !== checks not equal data type too
 if(b1!==b2){console.log("not equal \n")} else{console.log("equal")};
// // OPERATORS , && ,!, ||
// // && both things must be true
 if(b1>1 && b1>=3){console.log("pass \n")} else{console.log("fail")};
// // || OR one thing must be true
 if(b1==1 || b1==2){console.log("right")} else{console.log("wrong")};
// // && ,|| ! operators
 if(b1!=1 && b1==3 || b1!=2){console.log("great")} else{console.log("not great \n")};
// // LOOPS
// // FOR

 for(let c1=1;c1<11;c1++)
{console.log(c1 +"\n")};

 let w1="why"
for(g=0;g<5;g++)
 {console.log(w1)};

for(g=0;g<6;g++){console.log("well done \n")};

// WHILE
let p1=0;
while(p1<=10){
// will run forever unless you have that ++..follow the syntax
console.log("please");p1++

};

let z1=1;
while(z1<=5){console.log(z1)
z1++};

// FOR LOOP  for list

let myList=['eggs','bread','milk']
var l1;
for(var l1=0;l1<=myList.length;l1++){
    console.log(myList[l1]);
}

//  starting from a point in the list..
  for(x=4;x<=10;x++){
      console.log(x);
  };
// if else statement
  if(a1<a2){
    console.log("well done") }
else{
    console.log("done bad")
};