let shopList = ["bread", "milk", "eggs"];
console.log(shopList);
// to change things in the array
shopList[0]="catfood";
console.log(shopList);
// to add things to the array
shopList.push("sasuage","banana");
console.log(shopList);
// to remove the last item in the array 
shopList.pop();
console.log(shopList);
// array[]
console.log("\n");

console.log(shopList[0]);
console.log("\n");


console.log(shopList.length);

console.log("\n");
// for loop
for (var x=0;x<shopList.length;x++){
    console.log(shopList[x]);
}
console.log("\n");
// forEACH loop
shopList.forEach(item=>{
    console.log(item);
});
// cant call objects with loops
// object {}
console.log("\n");


let class1={jud: "one",uy:"two",ty:"three",
            jes:["apple","pear","grape"]
           };
class1.jes.forEach(i=>{
    console.log(i);
})
console.log("\n");

for (let z=0;z<class1.jes.length;z++){
    console.log(class1.jes[z]);
};
console.log("\n");

console.log(class1.jes[2]);

let object1={row1:"one",
             row2:"two",
            row3:"three"   };
console.log(object1);
console.log(object1.row3);